#include <iostream>

using namespace std;

const int MAX = 10;
char arr[MAX];

void Solve(int n, int curr)
{
    if (curr == n)
    {
        for (int i = 0; i < n; ++i) cout << arr[i];
        cout << endl;
        return;
    }

    for (int i = 0; i < 2; ++i)
    {
        arr[curr] = '0' + i;
        Solve(n, curr + 1);
    }
}

void Solve(int n)
{
    Solve(n, 0);
}

void RunTest()
{
    Solve(2);
    Solve(4);
}

int main(int argc, char** argv)
{
    if (argc > 1)
    {
        cout << "Test mode!" << endl;
        RunTest();
        return 0;
    }

    int n = 0;
    cin >> n;

    Solve(n);

    return 0;
}

