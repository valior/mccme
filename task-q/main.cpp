#include <iostream>

using namespace std;

const int MAX = 100;
int arr[MAX];

int calls = 0;

void Print(int len)
{
    for (int i = 0; i < len; ++i) cout << arr[i] << " ";
    cout << endl;
}

void Solve(int curr, int sum, int limit, int count, int amount)
{
    if (sum == limit && count == amount)
    {
        Print(curr);
        return;
    }

    if (curr == limit)
    {
        // should never get here
        cout << "Current index reach limit" << endl;
        Print(curr);
        return;
    }

    for (int i = 1; i <= limit; ++i)
    {
        ++calls;

        if (arr[curr-1] > i) continue;
        if (sum + i > limit) return;
        if (count + 1 > amount) return;

        arr[curr] = i;
        Solve(curr + 1, sum + i, limit, count + 1, amount);
    }
}

void Solve(int n, int k)
{
    if (n > MAX)
    {
        cout << "N is more then MAX" << endl;
        return;
    }

    if (k == 1)
    {
        cout << n << endl;
    }
    else if (n == k)
    {
        for (int i = 0; i < n; ++i) cout << "1 ";
        cout << endl;
    }
    else
    {
        Solve(0, 0, n, 0, k);
    }
}

void RunTests()
{
    cout << __func__ << endl;

    calls = 0;
    Solve(8, 3);
    cout << "calls=" << calls << endl;

    calls = 0;
    Solve(8, 5);
    cout << "calls=" << calls << endl;

    calls = 0;
    Solve(8, 1);
    cout << "calls=" << calls << endl;

    calls = 0;
    Solve(8, 8);
    cout << "calls=" << calls << endl;
}

int main(int argc, char** argv)
{
    if (argc > 1)
    {
        RunTests();
        return 0;
    }

    int n = 0, k = 0;
    cin >> n >> k;

    Solve(n, k);

    return 0;
}
