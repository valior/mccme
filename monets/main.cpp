#include <iostream>

using namespace std;

const int MAX = 32;
int arr[MAX];
int tmp[MAX];

int res[MAX];
int amount = MAX;

void Add(int cur, int len, int cur_sum, int sum, int i)
{
    if (cur_sum == sum)
    {
        if (amount > i)
        {
            amount = i;

            for (int j = 0; j < i; ++j) res[j] = tmp[j];
        }

        return;
    }

    if (cur == len)
    {
        return;
    }

    Add(cur + 1, len, cur_sum, sum, i);
    tmp[i] = arr[cur];
    Add(cur + 1, len, cur_sum + arr[cur], sum, i + 1);
    tmp[i+1] = arr[cur];
    Add(cur + 1, len, cur_sum + arr[cur] + arr[cur], sum, i + 2);
}

void Solve(int sum, int len)
{
    do
    {
        // Case 1
        int total_sum = 0;

        for (int i = 0; i < len; ++i) total_sum += arr[i];
        for (int i = 0; i < len; ++i) total_sum += arr[i];

        if (total_sum < sum)
        {
            amount = -1;
            break;
        }

        // Case 2
        amount = MAX;
        Add(0, len, 0, sum, 0);

        // Case 3
        if (amount == MAX)
        {
            amount = 0;
        }
    }
    while (false);

    cout << amount << endl;
    for (int j = 0; j < amount; ++j) cout << res[j] << " ";
    cout << endl;
}

void Sample1()
{
    cout << __func__ << endl;

    int n = 100, m = 6;
    int smpl[MAX] = {11, 20, 30, 40, 11, 99};

    for (int i = 0; i < m; ++i) arr[i] = smpl[i];

    Solve(n, m);
}

void Sample2()
{
    cout << __func__ << endl;

    int n = 100, m = 6;
    int smpl[MAX] = {11, 21, 31, 41, 11, 99};

    for (int i = 0; i < m; ++i) arr[i] = smpl[i];

    Solve(n, m);
}

void Sample3()
{
    cout << __func__ << endl;

    int n = 500, m = 6;
    int smpl[MAX] = {11, 21, 31, 41, 11, 99};

    for (int i = 0; i < m; ++i) arr[i] = smpl[i];

    Solve(n, m);
}

void Sample4()
{
    cout << __func__ << endl;

    int n = 20, m = 1;
    int smpl[MAX] = {10};

    for (int i = 0; i < m; ++i) arr[i] = smpl[i];

    Solve(n, m);
}


void RunTests()
{
    cout << __func__ << endl;

    Sample1();
    Sample2();
    Sample3();
    Sample4();
}

int main(int argc, char** argv)
{
    if (argc > 1)
    {
        RunTests();
        return 0;
    }

    int n = 0, m = 0;
    cin >> n >> m;

    for (int i = 0; i < m; ++i) cin >> arr[i];

    Solve(n, m);

    return 0;
}
