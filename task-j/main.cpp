#include <iostream>

using namespace std;

const int MAX = 100;
int arr[MAX];

void Solve(int curr, int len, int prev, int limit)
{
    if (curr == len)
    {
        for (int i = 0; i < len; ++i) cout << arr[i] << " ";
        cout << endl;
        return;
    }

    for (int i = prev; i <= limit; ++i)
    {
        arr[curr] = i;
        Solve(curr + 1, len, i + 1, limit);
    }
}

void Solve(int n, int k)
{
    if (n > MAX)
    {
        cout << "Too big input number" << endl;
        return;
    }

    Solve(0, k, 1, n);
}

void RunTests()
{
    cout << __func__ << endl;

    Solve(5, 2);
}

int main(int argc, char** argv)
{
    if (argc > 1)
    {
        RunTests();
        return 0;
    }

    int n = 0, k = 0;
    cin >> n >> k;

    Solve(n, k);

    return 0;
}

