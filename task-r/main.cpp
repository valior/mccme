#include <iostream>

using namespace std;

const int MAX = 10;
int buff[MAX];
bool used[MAX];

void Print(int len)
{
    for (int i = 0; i < len; ++i) cout << buff[i];
    cout << endl;
}

void Solve(int curr, int len)
{
    if (curr == len)
    {
        Print(len);
        return;
    }

    for (int i = 1; i <= len; ++i)
    {
        if (used[i] == true) continue;

        buff[curr] = i;
        used[i] = true;

        Solve(curr + 1, len);
        used[i] = false;
    }
}

void Solve(int n)
{
    Solve(0, n);
}

void RunTests()
{
    cout << __func__ << endl;

    Solve(3);
}

int main(int argc, char** argv)
{
    if (argc > 1)
    {
        RunTests();
        return 0;
    }

    int n = 0;
    cin >> n;

    Solve(n);

    return 0;
}
