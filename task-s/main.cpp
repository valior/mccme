#include <iostream>

using namespace std;

const int MAX = 11 * 2;
char buff[MAX];
char arr[] = {'(', ')'};

void Solve(int curr, int len, int count)
{
    if (curr == len*2 && count == 0)
    {
        for (int i = 0; i < len*2; ++i) cout << buff[i];
        cout << endl;
        return;
    }

    for (int i = 0; i < 2; ++i)
    {
        if (count < 0) return;
        if (curr == len*2) return;

        buff[curr] = arr[i];

        if (i == 0) Solve(curr + 1, len, count + 1);
        if (i == 1) Solve(curr + 1, len, count - 1);
    }
}

void Solve(int n)
{
    Solve(0, n, 0);
}

void RunTests()
{
    cout << __func__ << endl;

    Solve(3);
}

int main(int argc, char** argv)
{
    if (argc > 1)
    {
        RunTests();
        return 0;
    }

    int n = 0;
    cin >> n;

    Solve(n);

    return 0;
}

