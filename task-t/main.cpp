#include <iostream>

using namespace std;

const int MAX = 300 * 2;
char buff[MAX];
char arr[] = "()";

void Solve(int curr, int len, int count, int limit)
{
    if (curr == len*2 && count == 0)
    {
        for (int i = 0; i < len*2; ++i) cout << buff[i];
        cout << endl;
        return;
    }

    for (int i = 0; i < 2; ++i)
    {
        if (count < 0) return;
        if (count > limit) return;
        if (curr == len*2) return;

        buff[curr] = arr[i];

        if (i == 0) Solve(curr + 1, len, count + 1, limit);
        if (i == 1) Solve(curr + 1, len, count - 1, limit);
    }
}

void Solve(int n, int k)
{
    Solve(0, n, 0, k);
}

void RunTests()
{
    cout << __func__ << endl;

    Solve(4, 2);
}

int main(int argc, char** argv)
{
    if (argc > 1)
    {
        RunTests();
        return 0;
    }

    int n = 0, k = 0;
    cin >> n >> k;

    Solve(n, k);

    return 0;
}

