#include <iostream>

using namespace std;

const int MAX = 1000;
int buff[MAX];

void Print(int len)
{
    for (int i = 0; i < len-1; ++i) cout << buff[i] << "*";
    cout << buff[len-1] << endl;
}

void Solve(int curr, int n, int m, int res)
{
    if (res == n)
    {
        Print(curr);
        return;
    }

    if (curr == n)
    {
        // should never get here
        cout << "Buffer bound reached" << endl;
        return;
    }

    for (int i = m; i <= n; ++i)
    {
        if (curr != 0 && buff[curr-1] > i) continue;
        if (res * i > n) return;

        buff[curr] = i;
        Solve(curr + 1, n, m, res * i);
    }
}

void Solve(int n, int m)
{
    for (int i = 0; i < n; ++i) buff[i] = 0;

    Solve(0, n, m, 1);
}

void Sample1()
{
    cout << __func__ << endl;
    Solve(4, 3);
}

void Sample2()
{
    cout << __func__ << endl;
    Solve(18, 3);
}

void Sample3()
{
    cout << __func__ << endl;
    Solve(25, 5);
}

void RunTests()
{
    cout << __func__ << endl;

    Sample1();
    Sample2();
    Sample3();
}

int main(int argc, char** argv)
{
    if (argc > 1)
    {
        RunTests();
        return 0;
    }

    int n = 0, m = 0;
    cin >> n >> m;

    Solve(n, m);

    return 0;
}

