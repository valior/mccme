#include <iostream>

using namespace std;

const int MAX = 40;
int arr[MAX];

void Solve(int curr, int sum, int limit)
{
    if (sum == limit)
    {
        for (int i = 0; i < curr; ++i) cout << arr[i] << " ";
        cout << endl;
        return;
    }

    if (curr == limit)
    {
        // should never get here
        cout << "Current index reach limit" << endl;
        return;
    }

    for (int i = 1; i <= limit; ++i)
    {
        if (curr != 0 && arr[curr-1] < i) return;
        if (sum + i > limit) return;

        arr[curr] = i;
        Solve(curr + 1, sum + i, limit);
    }
}

void Solve(int n)
{
    if (n > MAX)
    {
        cout << "N is more then MAX" << endl;
        return;
    }

    Solve(0, 0, n);
}

void RunTests()
{
    cout << __func__ << endl;

    Solve(5);
}

int main(int argc, char** argv)
{
    if (argc > 1)
    {
        RunTests();
        return 0;
    }

    int n = 0;
    cin >> n;

    Solve(n);

    return 0;
}
