#include <iostream>

using namespace std;

const int MAX = 2000;
char arr[MAX];

void Print(int len)
{
    for (int i = 0; i < len; ++i) cout << char('0' + arr[i]) << " ";
    cout << endl;
}

void Solve(int curr, int n, int count, int k)
{
    if (curr == n)
    {
        Print(n);
        return;
    }

    for (int i = 0; i < 2; ++i)
    {
        // не содержащие двух единиц подряд
        if (curr != 0 && i == 1 && arr[curr-1] == 1) return;

        // не более k единиц
        if (i == 1 && count+1 > k) return;

        arr[curr] = i;

        Solve(curr + 1, n, count + i, k);
    }
}

void Solve(int n, int k)
{
    if (n > MAX)
    {
        cout << "Error: Limit is exceeded" << endl;
        return;
    }

    Solve(0, n, 0, k);
}

void RunTests()
{
    cout << __func__ << endl;

    Solve(4, 2);
}

int main(int argc, char** argv)
{
    if (argc > 1)
    {
        RunTests();
        return 0;
    }

    int n = 0, k = 0;
    cin >> n >> k;

    Solve(n, k);

    return 0;
}
