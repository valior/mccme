#include <iostream>

using namespace std;

const int MAX = 20;
char arr[MAX];

void Print(int len)
{
    for (int i = 0; i < len; ++i) cout << char('0' + arr[i]) << " ";
    cout << endl;
}

void Solve(int curr, int n)
{
    if (curr == n)
    {
        Print(n);
        return;
    }

    for (int i = 0; i < 2; ++i)
    {
        // не содержащие двух единиц подряд
        if (curr != 0 && i == 1 && arr[curr-1] == 1) return;

        arr[curr] = i;
        Solve(curr + 1, n);
    }
}

void Solve(int n)
{
    if (n > MAX)
    {
        cout << "Error: Limit is exceeded" << endl;
        return;
    }

    Solve(0, n);
}

void RunTests()
{
    cout << __func__ << endl;

    Solve(3);
}

int main(int argc, char** argv)
{
    if (argc > 1)
    {
        RunTests();
        return 0;
    }

    int n = 0;
    cin >> n;

    Solve(n);

    return 0;
}
