#include <iostream>

using namespace std;

const int MAX = 10;
char arr[MAX];

void Solve(int n, int k, int curr)
{
    if (curr == n)
    {
        for (int i = 0; i < n; ++i)
        {
            if (arr[i] < 10) cout << char('0' + arr[i]);
            else cout << char('a' + (arr[i] - 10));
        }
        cout << endl;
        return;
    }

    for (int i = k - 1; i >= 0; --i)
    {
        arr[curr] = i;

        Solve(n, k, curr + 1);
    }
}

void Solve(int n, int k)
{
    Solve(n, k, 0);
}

void RunTest()
{
    Solve(2, 2);
    Solve(3, 12);
}

int main(int argc, char** argv)
{
    if (argc > 1)
    {
        cout << "Test mode!" << endl;
        RunTest();
        return 0;
    }

    int n = 0, k = 0;
    cin >> n >> k;

    Solve(n, k);

    return 0;
}
