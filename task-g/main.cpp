#include <iostream>

using namespace std;

const int MAX = 1000;
char arr[MAX];

void Print(int len)
{
    for (int i = 0; i < len; ++i) cout << char('0' + arr[i]);
    cout << endl;
}

void Solve(int curr, int n, int count0, int count1, int k)
{
    if (curr == n && count1 == k)
    {
        Print(n);
        return;
    }

    for (int i = 0; i < 2; ++i)
    {
        // содержащие ровно K единиц
        if (i == 0 && count0+1 > n-k) continue;
        if (i == 1 && count1+1 > k) return;

        arr[curr] = i;

        if (i == 0) Solve(curr + 1, n, count0 + 1, count1 + i, k);
        else        Solve(curr + 1, n, count0, count1 + i, k);
    }
}

void Solve(int n, int k)
{
    if (n > MAX)
    {
        cout << "Error: Limit is exceeded" << endl;
        return;
    }

    Solve(0, n, 0, 0, k);
}

void RunTests()
{
    cout << __func__ << endl;

    Solve(4, 2);
}

int main(int argc, char** argv)
{
    if (argc > 1)
    {
        RunTests();
        return 0;
    }

    int n = 0, k = 0;
    cin >> n >> k;

    Solve(n, k);

    return 0;
}
